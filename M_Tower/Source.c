#include <stdio.h>

void hanoi(size_t rings, char from, char to, char via);
int main(){
	hanoi(12, 'a','c','b');
	getchar();
	return 0;
}

/*
prints ring moving process for tower of hanoi problem
in: size of tower, names for the three towers
out: none
Algorithim:
"move all except bottom to 2, move bottom to 3 and then move rest to three"
do this for each "level" of rings
*/
void hanoi(size_t rings, char from, char to, char via) {
	if (rings == 1) {
		printf("Move ring %d from %c to %c\n", rings, from, to);
	}
	else {
		hanoi(rings - 1, from, via, to);
		printf("Move ring %d from %c to %c\n", rings, from, to);
		hanoi(rings - 1, via, to, from);
	}

}